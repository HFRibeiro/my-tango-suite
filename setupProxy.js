const createProxyMiddleware = require("http-proxy-middleware");
const config = require("./config.json");

const proxies = {
  "/testdb/socket": {
    target: "http://localhost:5004",
    secure: false,
    changeOrigin: true,
    ws: true,
    pathRewrite: {
      '^/testdb': '',
    },
  },
  "/testdb/db": {
    target: "http://localhost:5004",
    secure: false,
    changeOrigin: true,
    pathRewrite: {
      '^/testdb': '',
    },
  },
  "/auth": {
    target: "http://localhost:8080",
    secure: false,
    changeOrigin: true,
    pathRewrite: {
      '^/auth': '',
    },
  },
  "/dashboards": {
    target: "http://localhost:3012",
    secure: false,
    changeOrigin: true,
  },
  "/logs": {
    target: "http://localhost:3012",
    secure: false,
    changeOrigin: true,
  },
  "/_search": {
    target: config.ELASTIC_URL,
    secure: false,
    changeOrigin: true,
  },
};

module.exports = function (app) {
  Object.entries(proxies).forEach(([endpoint, setting]) => {
    app.use(createProxyMiddleware(endpoint, setting));
  });
};
