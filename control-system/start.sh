#!/bin/bash

source /opt/conda/etc/profile.d/conda.sh
conda activate tango

# Function to check if a host:port is available
wait_for_host_port() {
    local host="$1"
    local port="$2"

    while ! nc -z "$host" "$port"; do
        echo "Waiting for $host:$port..."
        sleep 1
    done
}

# Use the function to wait for MySQL
wait_for_host_port tango-mysql 3306

# Continue with your script
Databaseds 2 -ORBendPoint giop:tcp:control-system:10000