## my-tango-suite

## This is a suite build to Linux ARM with tango-controls and taranta suite
## Using the following ports:
```
mysql: 3306
tango_host: 10000
tangogql: 5004
phpmyadmin: 81
taranta 80
taranta-auth 8080
taranta-dashboard 3012
logs 3012
```

## To start the services run
```
make up
```

## To stop the services run
```
make down
```

## to check tango database acess with phpmyadmin:

http://localhost

```
server: 127.0.0.1
user:   tango
pass:   tango
```

## Test tangogql
http://localhost:5004/graphiql/

```
query {
  devices(pattern: "sys*"){
    name
  }
}
```
