ARCH := $(shell uname -m)
ifeq ($(ARCH),x86_64)
  PHPMYADMIN_IMAGE := phpmyadmin/phpmyadmin
else ifeq ($(ARCH),aarch64)
  PHPMYADMIN_IMAGE := arm64v8/phpmyadmin
else ifeq ($(ARCH),arm64)
  PHPMYADMIN_IMAGE := arm64v8/phpmyadmin
else
  $(error Unknown architecture)
endif

export PHPMYADMIN_IMAGE

run: tangogql taranta taranta-auth taranta-dashboard update-nginx build-images
	docker-compose up

silent: tangogql taranta
	docker-compose up --build -d

build-images:
	cd ./tango-mysql  && docker build . -t tango-mysql
	cd ./control-system  && docker build . -t control-system
	cd ./tango-test  && docker build . -t tango-test
	cd ./tangogql  && docker build . -t tangogql
	cd ./taranta && docker build . -t taranta
	cd ./taranta-auth  && docker build . -t taranta-auth
	cd ./taranta-dashboard  && docker build . -t taranta-dashboard

scout-images:
	mkdir -p ./scout
	docker scout cves --format markdown -o scout/tango-mysql.md local://tango-mysql:latest
	docker scout cves --format markdown -o scout/control-system.md local://control-system:latest
	docker scout cves --format markdown -o scout/tango-test.md local://tango-test:latest
	docker scout cves --format markdown -o scout/tangogql.md local://tangogql:latest
	docker scout cves --format markdown -o scout/taranta.md local://taranta:latest
	docker scout cves --format markdown -o scout/taranta-auth.md local://taranta-auth:latest
	docker scout cves --format markdown -o scout/taranta-dashboard.md local://taranta-dashboard:latest

tangogql:
	if [ ! -d "tangogql" ]; then \
		git clone https://gitlab.com/tango-controls/web/tangogql.git; \
		cd tangogql && git checkout tangogql-arm; \
	fi

taranta:
	if [ ! -d "taranta" ]; then \
		git clone https://gitlab.com/tango-controls/web/taranta.git; \
		cd taranta && git checkout SP-3183-refactor-dashboards; \
	fi

taranta-auth:
	if [ ! -d "taranta-auth" ]; then \
		git clone https://gitlab.com/tango-controls/web/taranta-auth.git; \
	fi

taranta-dashboard:
	if [ ! -d "taranta-dashboard" ]; then \
		git clone https://gitlab.com/tango-controls/web/taranta-dashboard.git; \
	fi

update-nginx:
	cp nginx.conf taranta/assets/nginx.conf

down:
	docker-compose down